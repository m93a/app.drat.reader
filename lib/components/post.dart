import 'package:flutter/material.dart';

class Post extends StatelessWidget {
  const Post({Key? key}) : super(key: key);

  @override
  Widget build(context) {
    return Card(
      margin: const EdgeInsets.only(top: 20),
      child: SizedBox(
        width: 600,
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Row(
            children: [
              const CircleAvatar(
                child: Text("Foo"),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: const [
                    Text("Tvoje mamki"),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

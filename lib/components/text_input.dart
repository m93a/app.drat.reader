import 'package:flutter/material.dart';

class TextInput extends StatelessWidget {
  const TextInput({Key? key, this.width, this.height = 30}) : super(key: key);

  final double? width;
  final double? height;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: TextField(
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.only(left: 5),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(4),
            borderSide: const BorderSide(),
          ),
        ),
      ),
    );
  }
}

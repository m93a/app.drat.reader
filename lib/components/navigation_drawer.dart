import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:reader/components/text_input.dart';

class NavHeader extends StatelessWidget {
  const NavHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 120,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(10),
            child: SvgPicture.asset(
              "assets/logo.svg",
              height: 100,
            ),
          ),
          ToggleButtons(
            children: const [
              Icon(Icons.favorite_border_sharp),
              Icon(Icons.access_time),
              Icon(Icons.rocket_launch_outlined), // TODO better icon
              Icon(Icons.more_horiz),
            ],
            isSelected: const [true, false, false, false],
            borderRadius: BorderRadius.circular(6),
            constraints: BoxConstraints.tight(const Size(40, 40)),
          ),
        ],
      ),
    );
  }
}

class NavSearch extends StatelessWidget {
  const NavSearch({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(25, 10, 15, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: const [
              Expanded(
                child: Padding(
                  padding: EdgeInsets.only(right: 10),
                  child: TextInput(),
                ),
              ),
              Icon(Icons.search),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 15, right: 50),
            child: Wrap(
              spacing: 5,
              runSpacing: 5,
              children: const [
                Chip(label: Text("News")),
                Chip(label: Text("Memes")),
                Chip(label: Text("Social")),
                Chip(label: Text("Cute")),
                Chip(label: Text("IT")),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class NavAttention extends StatelessWidget {
  const NavAttention({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: const [
          Text("Attention span:"),
          TextInput(
            width: 80,
          ),
        ],
      ),
    );
  }
}

class NavItem extends StatelessWidget {
  const NavItem({
    Key? key,
    required this.title,
    required this.icon,
    this.widget,
  }) : super(key: key);

  final String title;
  final IconData icon;
  final Widget? widget;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(icon),
      title: Text(title),
      onTap: () {
        if (widget == null) return;
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => widget!),
        );
      },
    );
  }
}

class NavDrawer extends StatelessWidget {
  const NavDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: const [
          NavHeader(),
          Divider(),
          NavSearch(),
          Divider(),
          NavAttention(),
          Divider(),
        ],
      ),
    );
  }
}
